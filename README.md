Bug report
==========

An example of a formatting bug for the Markdown-auto-generated [TOC] - the
margins are obviously not properly set for the nested `<ul>` items.

[TOC]


Item 1
-------

Foobar.


Item 2
-------

Foobar.


### Item 2.1

Foobar.


### Item 2.2

Foobar.


#### Item 2.2.1

Foobar.


#### Item 2.2.2

Foobar.


### Item 2.3

Foobar.


Item 3
--------

Foobar.


Added comment to demonstrate using Emoji in commit messages (which breaks the
layout of the commit listing page a bit).
